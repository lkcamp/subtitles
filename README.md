# Subtitles for the LKCAMP streams

This project will have the subtitles used on for the stream videos of the
meeting that are on youtube.

## Guide

* The files must be in sbv format.
* Just create a directory with the meeting name like M1 for the first meeting.
* And name de files as the meeting name "-" the language like M1-en or M1-pt_BR
inside the respective directory
